clc
newXsb=zeros(20,1);
for i1=1:20
    disp(i1)
    name=sprintf('test%d.mat',i1);
    name=fullfile('500stepsimporved',name);
    load(name)
    fprintf('Value of c/D_0=%1.8f\n',xsb(3)/xsb(4))
    fprintf('Xsb=%1.4f',xsb)
    disp(xsbFunctionValue)
    newXsb(i1)=xsbFunctionValue;
end
[argValue,maxarg]=max(newXsb);
disp(maxarg)
%load(sprintf('test%d.mat',6))
load(fullfile('500stepsimporved','test6.mat'))
newImage=updatePicute(image,xsb(1),xsb(2),xsb(3),xsb(4));
figure(1)
imshow(newImage)
title('Enhanced image, second best')
saveas(gca,'Enhancedimage.png')
figure(2)
imshow(image)
title('Origninal image')
saveas(gca,'OrignialImage.png')