function position = InitializePosition(sizeOfSwarm,numberOfVariables,xmin,xmax)
  position=zeros(sizeOfSwarm,numberOfVariables);
  for i=1:sizeOfSwarm
    for j=1:numberOfVariables
      r=rand;
      position(i,j)=xmin(j)+r*(xmax(j)-xmin(j));
    end
  end

end

