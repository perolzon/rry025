function position=UpdatePositions(position,velocities,deltaT,sizeOfSwarm,numberOfVariables,xmax,xmin)
  for i=1:sizeOfSwarm
    for j=1:numberOfVariables
      position(i,j)=position(i,j)+velocities(i,j)*deltaT;
      if(position(i,j)>xmax(j))
         position(i,j)=xmax(j); 
      end
      if(position(i,j)<xmin(j))
         position(i,j)=xmin(j);
      end
    end
  end
end