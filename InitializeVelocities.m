function velocities = InitializeVelocities( sizeOfSwarm,numberOfVariables,xmin,xmax,alpha,deltaT )
  velocities=zeros(sizeOfSwarm,numberOfVariables);
  for i=1:sizeOfSwarm
    for j=1:numberOfVariables
      r=rand;
      velocities(i,j)=alpha/deltaT*(xmin(j)+r*(xmax(j)-xmin(j)));
    end
  end


end

