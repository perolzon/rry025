function [paddedImage,paddedSize] = GetPaddedImage(image)
    imageSize=size(image);
    %Pad the image and set the homomorphic filter
    exponentOfX=log2(imageSize(1));
    exponentOfY=log2(imageSize(2));
    paddedXPixels=2^(ceil(exponentOfX))-imageSize(1);
    paddedYPixels=2^(ceil(exponentOfY))-imageSize(2);
    paddedSize=2^(ceil(exponentOfX));
    paddedImage=padarray(image,[paddedXPixels paddedYPixels],'post');
end

