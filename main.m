clear all,clc,close all

image=load('forest.mat');
image=image.forestgray;

sizeOfSwarm=30;%20-40
initialSpeed=0.03;%Close to 0
%Use different ranges for different variables.
gammaH=[1.01,3];
gammaL=[0,0.9];
c=[0.5,5];
D0=[20,6e2];
tic
xmin=[gammaH(1) gammaL(1) c(1) D0(1)];
xmax=[gammaH(2) gammaL(2) c(2) D0(2)];
numberOfVariables=4;
alpha=1;
deltaT=1;
numberOfSteps=500;
inertiaWeight=1.4;
lowestInertiaWeight=0.3;
beta=0.999;
c1=2;
c2=2;
for nameVariable=1:20
position=InitializePosition(sizeOfSwarm,numberOfVariables,xmin,xmax);
velocities=InitializeVelocities( sizeOfSwarm,numberOfVariables,xmin,xmax,alpha,deltaT );

xpb=repmat(xmax./2,sizeOfSwarm,1);
xsb=xmax./2;
for i=1:sizeOfSwarm
  xpbFunctionValue(i)=EvaluateParticle(image,xpb(i,:));
end
xsbFunctionValue=EvaluateParticle(image,xsb);
j=0;
while(j<numberOfSteps)
  parfor i=1:sizeOfSwarm
    functionValue=EvaluateParticle(image,position(i,:));
    
    if(functionValue>xpbFunctionValue(i))
      xpbFunctionValue(i)=functionValue;
      xpb(i,:)=position(i,:);
    end
    
%     if(functionValue<xsbFunctionValue)
%       xsbFunctionValue=functionValue;
%       xsb=position(i,:);
%     end
  end
  if(max(xpbFunctionValue)>xsbFunctionValue)
      [argValue,argmax]=max(xpbFunctionValue);
      xsbFunctionValue=argValue;
      xsb=position(argmax,:);
  end
  velocities=UpdateVelocities(velocities,position,xpb,xsb,deltaT,c1,c2,sizeOfSwarm,numberOfVariables,inertiaWeight);
  position=UpdatePositions(position,velocities,deltaT,sizeOfSwarm,numberOfVariables,xmax,xmin);
  if(inertiaWeight>lowestInertiaWeight)
    inertiaWeight=beta*inertiaWeight;
  end
  j=j+1;
end
name=sprintf('test%d.mat',nameVariable);
save(name)
end
%%
disp(xsbFunctionValue)
newImage=updatePicute(image,xsb(1),xsb(2),xsb(3),xsb(4));
figure(1)
imshow(newImage)
title('Processed image')
figure(2)
histogram(newImage)
title('Processed image')
figure(3)
imshow(image)
title('Old image')
figure(4)
histogram(image)
title('Old image')
toc