function H=homomorphicFilter(gammaH,gammaL,c,D,D0)
    H=(gammaH-gammaL)*(1-exp((-c*(D./D0).^2)))+gammaL;
end