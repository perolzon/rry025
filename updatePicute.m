function newImage = updatePicute(image,gammaH,gammaL,c,D0)
    imageSize=size(image);
    %Pad the image and set the homomorphic filter
    [paddedImage,paddedImageSize]=GetPaddedImage(image);
    
    [X,Y]=meshgrid(1:paddedImageSize,1:paddedImageSize);
    centerX=ceil(paddedImageSize/2);
    centerY=ceil(paddedImageSize/2);
    D=sqrt((X-centerX).^2+(Y-centerY).^2);

    filter=homomorphicFilter(gammaH,gammaL,c,D,D0);
    % figure(3)
    % imshow(filter)

    %Fourier transform of picture
    z=log(1+paddedImage);
    fftZ=fft2(z);
    fftImageWithFilter=filter.*fftZ;
    newImage=ifft2(fftImageWithFilter);
    newImage=newImage(1:imageSize(1),1:imageSize(2));
    newImage=real(exp(newImage)-1);
    %Normilize
    newImage=newImage./max(max(newImage));
end

