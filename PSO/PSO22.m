clear all, clc,clf
sizeOfSwarm=30;%20-40
initialSpeed=0.03;%Close to 0
xmin=-5;
xmax=5;
numberOfVariables=2;
alpha=1;
deltaT=1;
numberOfStepsInPlot=1000;
inertiaWeight=1.4;
lowestInertiaWeight=0.3;
beta=0.99;
c1=2;
c2=2;

position=InitializePosition(sizeOfSwarm,numberOfVariables,xmin,xmax);
velocities=InitializeVelocities( sizeOfSwarm,numberOfVariables,xmin,xmax,alpha,deltaT );

xpb=repmat(xmax,sizeOfSwarm,2);
xsb=[xmax xmax];
for i=1:sizeOfSwarm
  xpbFunctionValue(i)=EvaluateParticle(xpb(i,:));
end
xsbFunctionValue=EvaluateParticle(xsb);
j=0;
while(j<10000)
  for i=1:sizeOfSwarm
    functionValue=EvaluateParticle(position(i,:));
    
    if(functionValue<xpbFunctionValue(i))
      xpbFunctionValue(i)=functionValue;
      xpb(i,:)=position(i,:);
    end
    
    if(functionValue<xsbFunctionValue)
      xsbFunctionValue=functionValue;
      xsb=position(i,:);
    end
  end
  
  velocities=UpdateVelocities(velocities,position,xpb,xsb,deltaT,c1,c2,sizeOfSwarm,numberOfVariables,inertiaWeight);
  position=UpdatePositions(position,velocities,deltaT,sizeOfSwarm,numberOfVariables);
  if(inertiaWeight>lowestInertiaWeight)
    inertiaWeight=beta*inertiaWeight;
  end
  j=j+1;
end

fprintf('x= %.6f \ny=%.6f \nFunction value=%1.6e \n',xsb(1),xsb(2),xsbFunctionValue)