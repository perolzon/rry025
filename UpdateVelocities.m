function velocities=UpdateVelocities(velocities,position,xpb,xsb,deltaT,c1,c2,sizeOfSwarm,numberOfVariables,inertiaWeight)
  
  for i=1:sizeOfSwarm
    for j=1:numberOfVariables
      r=rand;
      q=rand;
      personalInfluence=c1*q*(xpb(i,j)-position(i,j))/deltaT;
      groupInfluence=c2*r*(xsb(j)-position(i,j))/deltaT;
      velocities(i,j)=inertiaWeight*velocities(i,j)...
        +personalInfluence ...
        + groupInfluence;
    end
  end
end