function PlotFunction()
  numberOfPoints=1000;
  tmp=linspace(-5,5,numberOfPoints);
  [X Y]=meshgrid(tmp,tmp);
  logFunctionValues=zeros(numberOfPoints,numberOfPoints);
  for i=1:numberOfPoints
    for j=1:numberOfPoints
      logFunctionValues(i,j)=log10(0.01+EvaluateParticle([X(i,j) Y(i,j)]));
    end
  end
  
  figure(1);
  contour(X,Y,logFunctionValues)
  xt=get(gca,'XTick');
  set(gca,'Fontsize',10)
  xlabel('x','FontSize',30)
  yt=get(gca,'YTick');
  ylabel('y','FontSize',30)
  set(gca,'Fontsize',15)
  
  title('Contour of the logaritmic value of the function','Fontsize',35)
  
  
end