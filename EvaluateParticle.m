function  fitness= EvaluateParticle(image,position)
    newImage=updatePicute(image,position(1),position(2),...
        position(3),position(4));
    e=entropy(newImage);
    cannyImage=edge(newImage,'Canny');
    %The three terms
    imageSize=size(newImage);
    numberOfEdgels=sum(sum(cannyImage));
    cannyIntesities=cannyImage.*newImage;
    sumOfCannyIntesities=sum(sum(cannyIntesities));
    H=log(log(sumOfCannyIntesities))*...
        numberOfEdgels/(imageSize(1)*imageSize(2))*...
        e;
    fitness=H;
    
%     [N,edges] = histcounts(newImage,256);
%     p=N./sum(N);
%     tmp=p.*log2(p);
%     tmp2=entropy(p);
%     tmp(isnan(tmp))=[];
%     
%     H=-sum(tmp);

end

